<?php
/**
 * Contract for DirectoryUtility
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper\Contracts;

use Symfony\Component\Console\Output\Output;

/**
 * Interface DirectoryUtilityContract
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Contracts
 */
interface DirectoryUtilityContract
{
    /**
     * @param string $path
     * @param Output|null $output
     */
    public function createDirectoryTreeIfNotExists(string $path, ?Output $output = null): void;
}
