<?php
/**
 * Contract for RepositoryService
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper\Contracts;

use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Contract;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Model;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Repository;

/**
 * Interface RepositoryServiceContract
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Contracts
 */
interface RepositoryServiceContract
{
    /**
     * @const string
     */
    public const CONTRACT_NAMESPACE = 'App\\Contracts\\Repositories';

    /**
     * @const string
     */
    public const REPOSITORY_NAMESPACE  = 'App\\Repositories';

    /**
     * @const string
     */
    public const CONTRACT_STUB_NAME_TEMPLATE = '{{CONTRACT_NAME}}';

    /**
     * @const string
     */
    public const CONTRACT_STUB_NAMESPACE_TEMPLATE = '{{CONTRACT_NAMESPACE}}';

    /**
     * @const string
     */
    public const REPOSITORY_STUB_NAME_TEMPLATE = '{{REPOSITORY_NAME}}';

    /**
     * @const string
     */
    public const REPOSITORY_STUB_NAMESPACE_TEMPLATE = '{{REPOSITORY_NAMESPACE}}';

    /**
     * @const string
     */
    public const CONTRACT_FQN_TEMPLATE = '{{CONTRACT_FQN}}';

    /**
     * @const string
     */
    public const MODEL_FQN_TEMPLATE = '{{MODEL_FQN}}';

    /**
     * @const string
     */
    public const MODEL_NAME_TEMPLATE = '{{MODEL_NAME}}';

    /**
     * @const string
     */
    public const MODEL_ROOT_NAMESPACE = 'App\\Models';

    /**
     * @param Model $model
     */
    public function ensureModelExists(Model $model): void;

    /**
     * @param Contract $contract
     * @return string
     */
    public function generateContractStub(Contract $contract): string;

    /**
     * @param Repository $repository
     * @return string
     */
    public function generateRepositoryStub(Repository $repository): string;

    /**
     * @param string $stub
     * @param string $filename
     */
    public function createFileFromStubContents(string $stub, string $filename): void;
}
