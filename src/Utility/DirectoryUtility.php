<?php
/**
 * Utility for directories/directory creation
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper\Utility;

use Gila\LaravelApiHelpers\RepositoryPatternHelper\Contracts\DirectoryUtilityContract;
use Illuminate\Foundation\Application;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Output\Output;

/**
 * Class DirectoryUtility
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Utility
 */
class DirectoryUtility implements DirectoryUtilityContract
{
    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var Application
     */
    protected $app;

    /**
     * DirectoryUtility constructor.
     * @param Filesystem $filesystem
     * @param Application $app
     */
    public function __construct(Filesystem $filesystem, Application $app)
    {
        $this->filesystem = $filesystem;
        $this->app = $app;
    }

    /**
     * @param string $path
     * @param Output|null $output
     */
    public function createDirectoryTreeIfNotExists(string $path, ?Output $output = null): void
    {
        $pathParts = explode('/', $path);

        $pathChain = null;
        foreach ($pathParts as $part) {
            if (str_contains($part, '.php')) {
                break;
            }

            $pathChain = sprintf('%s/%s', $pathChain, $part);
            if (!$this->filesystem->isDirectory($this->app->path() . $pathChain)) {
                $this->output(sprintf('Creating directory %s', $pathChain));
                $this->filesystem->makeDirectory($this->app->path() . $pathChain);
            }
        }
    }

    /**
     * @param string $message
     * @param Output|null $output
     */
    protected function output(string $message, ?Output $output = null): void
    {
        if (!is_null($output)) {
            $output->writeln($message);
        }
    }
}
