<?php
/**
 * Service to generate repositories and their contracts
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper\Services;

use Gila\LaravelApiHelpers\RepositoryPatternHelper\Contracts\DirectoryUtilityContract;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Contracts\RepositoryServiceContract;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Contract;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Model;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Repository;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Application;

/**
 * Class RepositoryService
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Services
 */
class RepositoryService implements RepositoryServiceContract
{
    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var Application
     */
    protected $app;

    /**
     * @var DirectoryUtilityContract
     */
    protected $directoryUtility;

    /**
     * @var string
     */
    protected $stubPath;

    /**
     * RepositoryService constructor.
     * @param Filesystem $filesystem
     * @param Application $app
     * @param DirectoryUtilityContract $directoryUtility
     * @param string $stubPath
     */
    public function __construct(Filesystem $filesystem, Application $app, DirectoryUtilityContract $directoryUtility, string $stubPath)
    {
        $this->filesystem = $filesystem;
        $this->app = $app;
        $this->directoryUtility = $directoryUtility;
        $this->stubPath = $stubPath;
    }

    /**
     * @param Model $model
     */
    public function ensureModelExists(Model $model): void
    {
        $model->ensureExists();
    }

    /**
     * @param Contract $contract
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function generateContractStub(Contract $contract): string
    {
        $template = [
            self::CONTRACT_STUB_NAME_TEMPLATE => $contract->getName(),
            self::CONTRACT_STUB_NAMESPACE_TEMPLATE => $contract->getNamespace(),
            self::REPOSITORY_STUB_NAME_TEMPLATE => $contract->getRepository()->getName()
        ];

        return $this->populateStubTemplate($this->getStub('contract.stub'), $template);
    }

    /**
     * @param Repository $repository
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function generateRepositoryStub(Repository $repository): string
    {
        $template = [
            self::REPOSITORY_STUB_NAME_TEMPLATE => $repository->getName(),
            self::REPOSITORY_STUB_NAMESPACE_TEMPLATE => $repository->getNamespace(),
            self::MODEL_FQN_TEMPLATE => $repository->getModel()->getFqn(),
            self::CONTRACT_FQN_TEMPLATE => $repository->getContract()->getFqn(),
            self::CONTRACT_STUB_NAME_TEMPLATE => $repository->getContract()->getName(),
            self::MODEL_NAME_TEMPLATE => $repository->getModel()->getName()
        ];

        return $this->populateStubTemplate($this->getStub('repository.stub'), $template);
    }

    /**
     * @param string $stub
     * @param string $filename
     */
    public function createFileFromStubContents(string $stub, string $filename): void
    {
        $this->directoryUtility->createDirectoryTreeIfNotExists($filename);
        $this->filesystem->put(
            sprintf(
                '%s/%s',
                $this->app->path(),
                $filename
            ), $stub);
    }

    /**
     * @param string $stubName
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function getStub(string $stubName): string
    {
        return $this->filesystem->get(sprintf('%s/%s', $this->stubPath, $stubName));
    }

    /**
     * @param string $stub
     * @param array $template
     * @return string
     */
    protected function populateStubTemplate(string $stub, array $template): string
    {
        return str_replace(array_keys($template), array_values($template), $stub);
    }
}
