<?php
/**
 * Domain model for creating a repository
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper\Models;

/**
 * Class Repository
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Models
 */
class Repository extends AbstractObjectModel
{
    /**
     * @var Contract
     */
    protected $contract;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Contract $contract
     */
    public function setContract(Contract $contract): void
    {
        $this->contract = $contract;
    }

    /**
     * @param Model $model
     */
    public function setModel(Model $model): void
    {
        $this->model = $model;
    }

    /**
     * @return Contract
     */
    public function getContract(): Contract
    {
        return $this->contract;
    }

    /**
     * @return Model
     */
    public function getModel(): Model
    {
        return $this->model;
    }
}
