<?php
/**
 * Handles domain models
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper\Models;

abstract class AbstractObjectModel
{
    /**
     * @var mixed
     */
    protected $name;

    /**
     * @var string
     */
    protected $namespace;

    /**
     * AbstractObjectModel constructor.
     * @param string $classFqn
     */
    public function __construct(string $classFqn)
    {
        $parts = explode('\\', $classFqn);
        $this->namespace = implode('\\', array_splice($parts, 0, count($parts) - 1));
        $this->name = end($parts);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    /**
     * @return string
     */
    public function getFqn(): string
    {
        return sprintf('%s\\%s', $this->getNamespace(), $this->getName());
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return sprintf('%s.php', $this->getName());
    }

    /**
     * @return string
     */
    public function getFqnFilename(): string
    {
        $fqnNamespace = $this->getNamespace();
        $parts = explode('\\', $fqnNamespace);
        $path = [];
        foreach ($parts as $index => $part) {
            if ($part === 'App' && $index === 0) {
                continue;
            }

            $path[] = $part;
        }

        $path = implode('/', $path);

        return sprintf('%s/%s', $path, $this->getFilename());
    }
}