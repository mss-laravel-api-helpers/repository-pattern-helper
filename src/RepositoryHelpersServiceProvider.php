<?php
/**
 * Service provider for package
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper;

use Gila\LaravelApiHelpers\RepositoryPatternHelper\Console\Commands\CreateRepositoryCommand;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Contracts\DirectoryUtilityContract;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Contracts\RepositoryServiceContract;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Services\RepositoryService;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Utility\DirectoryUtility;
use Illuminate\Support\ServiceProvider;

/**
 * Class RepositoryHelpersServiceProvider
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper
 */
class RepositoryHelpersServiceProvider extends ServiceProvider
{
    /**
     * Boot the service provider
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                CreateRepositoryCommand::class,
            ]);
        }

        $this->app->bind(DirectoryUtilityContract::class, DirectoryUtility::class);
        $this->app->bind(RepositoryServiceContract::class, RepositoryService::class);

        $this->app->when(RepositoryService::class)->needs('$stubPath')->give(function (): string {
            return __DIR__ . '/../stubs';
        });
    }
}