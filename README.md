# Repository Pattern Helper

### About

This package is intended to be used with [nwilging/eloquent-repositories](https://bitbucket.org/mss-laravel-api-helpers/eloquent-repositories).
You may use this tool to quickly generate a repository structure for a given model. This command
will generate the `RepositoryContract` and the `Repository`.

### Installation

Add the following to your `composer.json`:
```
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/mss-laravel-api-helpers/repository-pattern-helper.git"
    }
]
```

Require via `composer`:

```
composer require --dev gila-api/repository-pattern-helper
```

**Current Version:**

```
composer require --dev gila-api/repository-pattern-helper v1.0.0
```

### Usage

This command is called with Laravel's `artisan`:

```
php artisan larapi:repository:make {model}
```

It is important that you store your models in a hierarchical structure within the `App\Models`
namespace. This is the default namespace that this package looks for models in. Models should
be stored in `app/Models`.

When providing `{model}` argument to the command, use the relative namespace of the model
within `App\Models`.

Example:

If my model was `App\Models\MyModel`, I would simple call `larapi:repository:make MyModel`.
If my model was `App\Models\SubPath\MyModel`, I would call `larapi:repository:make SubPath\MyModel`.