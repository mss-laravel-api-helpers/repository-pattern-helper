<?php
/**
 * Test for model Model
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\Unit\Models;

use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Model;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\TestCase;

/**
 * Class ModelTest
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\Unit\Models
 */
class ModelTest extends TestCase
{
    public function testNamespaceParsedCorrectly()
    {
        $model = new Model('App\\Models\\Model');

        $this->assertSame('App\\Models', $model->getNamespace());
        $this->assertSame('Model', $model->getName());
        $this->assertSame('App\\Models\\Model', $model->getFqn());
        $this->assertSame('Model', $model->getRelativeModel());
    }

    public function testFilesystemParsedCorrectly()
    {
        $model = new Model('App\\Models\\Model');

        $this->assertSame('Model.php', $model->getFilename());
        $this->assertSame('Models/Model.php', $model->getFqnFilename());
    }

    public function testEnsureModelExistsWhenModelExists()
    {
        $model = new Model(\Illuminate\Database\Eloquent\Model::class);
        $model->ensureExists();
        $this->assertTrue(true);
    }

    public function testEnsureModelExistsThrowsExceptionWhenModelDoesNotExist()
    {
        $this->expectException(\RuntimeException::class);

        $model = new Model('Some\\Nonexistent\\Model');
        $model->ensureExists();
    }
}
