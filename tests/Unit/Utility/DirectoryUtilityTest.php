<?php
/**
 * Test for DirectoryUtility
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\Unit\Utility;

use Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\TestCase;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Utility\DirectoryUtility;
use Illuminate\Foundation\Application;
use Illuminate\Filesystem\Filesystem;
use Mockery\MockInterface;

/**
 * Class DirectoryUtilityTest
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\Unit\Utility
 */
class DirectoryUtilityTest extends TestCase
{
    /**
     * @var string
     */
    protected $appBasePath = '/base/path/app';

    /**
     * @var MockInterface
     */
    protected $appMock;

    /**
     * @var MockInterface
     */
    protected $filesystem;

    /**
     * @var DirectoryUtility
     */
    protected $utility;

    protected function setUp(): void
    {
        parent::setUp();

        $this->appMock = \Mockery::mock(Application::class);
        $this->filesystem = \Mockery::mock(Filesystem::class);

        $this->appMock->shouldReceive('path')->andReturn($this->appBasePath);
        $this->appMock->shouldReceive('flush');

        $this->utility = new DirectoryUtility($this->filesystem, $this->appMock);
    }

    public function testCreateDirectoryTreeIfNotExistsSuccess()
    {
        $path = 'Path/Within/App';

        $this->filesystem->shouldReceive('isDirectory')
            ->once()
            ->with($this->appBasePath . '/Path')
            ->andReturnFalse();
        $this->filesystem->shouldReceive('isDirectory')
            ->once()
            ->with($this->appBasePath . '/Path/Within')
            ->andReturnFalse();
        $this->filesystem->shouldReceive('isDirectory')
            ->once()
            ->with($this->appBasePath . '/Path/Within/App')
            ->andReturnFalse();

        $this->filesystem->shouldReceive('makeDirectory')
            ->once()
            ->with($this->appBasePath . '/Path');
        $this->filesystem->shouldReceive('makeDirectory')
            ->once()
            ->with($this->appBasePath . '/Path/Within');
        $this->filesystem->shouldReceive('makeDirectory')
            ->once()
            ->with($this->appBasePath . '/Path/Within/App');

        $this->utility->createDirectoryTreeIfNotExists($path);
    }

    public function testCreateDirectoryTreeIfNotExistsOnlyCreatesNonExistentDirectories()
    {
        $path = 'Path/Within/App';

        $this->filesystem->shouldReceive('isDirectory')
            ->once()
            ->with($this->appBasePath . '/Path')
            ->andReturnTrue();
        $this->filesystem->shouldReceive('isDirectory')
            ->once()
            ->with($this->appBasePath . '/Path/Within')
            ->andReturnFalse();
        $this->filesystem->shouldReceive('isDirectory')
            ->once()
            ->with($this->appBasePath . '/Path/Within/App')
            ->andReturnFalse();

        $this->filesystem->shouldReceive('makeDirectory')
            ->once()
            ->with($this->appBasePath . '/Path/Within');
        $this->filesystem->shouldReceive('makeDirectory')
            ->once()
            ->with($this->appBasePath . '/Path/Within/App');

        $this->utility->createDirectoryTreeIfNotExists($path);
    }

    public function testCreateDirectoryTreeIfNotExistsSkipsFiles()
    {
        $path = 'Path/Class.php';

        $this->filesystem->shouldReceive('isDirectory')
            ->once()
            ->with($this->appBasePath . '/Path')
            ->andReturnFalse();

        $this->filesystem->shouldReceive('makeDirectory')
            ->once()
            ->with($this->appBasePath . '/Path');

        $this->utility->createDirectoryTreeIfNotExists($path);
    }
}
