<?php
/**
 * Test for CreateRepositoryCommand
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\Unit\Commands;

use Gila\LaravelApiHelpers\RepositoryPatternHelper\Console\Commands\CreateRepositoryCommand;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Contracts\RepositoryServiceContract;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Contract;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Model;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Models\Repository;
use Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\TestCase;
use Illuminate\Console\OutputStyle;
use Mockery\MockInterface;
use Symfony\Component\Console\Input\Input;

/**
 * Class CreateRepositoryCommandTest
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests\Unit\Commands
 */
class CreateRepositoryCommandTest extends TestCase
{
    /**
     * @var CreateRepositoryCommand
     */
    protected $command;

    /**
     * @var MockInterface
     */
    protected $input;

    /**
     * @var MockInterface
     */
    protected $output;

    /**
     * @var MockInterface
     */
    protected $repositoryService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->input = \Mockery::mock(Input::class);
        $this->output = \Mockery::mock(OutputStyle::class);
        $this->repositoryService = \Mockery::mock(RepositoryServiceContract::class);

        $this->command = new CreateRepositoryCommand($this->repositoryService);

        $reflected = new \ReflectionClass($this->command);

        $input = $reflected->getProperty('input');
        $input->setAccessible(true);
        $input->setValue($this->command, $this->input);

        $output = $reflected->getProperty('output');
        $output->setAccessible(true);
        $output->setValue($this->command, $this->output);
    }

    public function testHandleSuccess()
    {
        $modelName = 'Path\\To\\Model';
        $this->input->shouldReceive('getArgument')->once()->with('model')->andReturn($modelName);

        $this->repositoryService->shouldReceive('ensureModelExists')
            ->once()
            ->with(\Mockery::on(function (Model $model) {
                $this->assertSame('App\\Models\\Path\\To\\Model', $model->getFqn());
                $this->assertSame('Path\\To\\Model', $model->getRelativeModel());
                return true;
            }));

        $this->repositoryService->shouldReceive('generateContractStub')
            ->once()
            ->with(\Mockery::on(function (Contract $contract) {
                $this->assertSame('ModelRepositoryContract', $contract->getName());
                $this->assertSame('App\\Contracts\\Repositories\\Path\\To\\ModelRepositoryContract', $contract->getFqn());
                return true;
            }))->andReturn('ContractStub');

        $this->repositoryService->shouldReceive('createFileFromStubContents')
            ->once()
            ->with('ContractStub', 'Contracts/Repositories/Path/To/ModelRepositoryContract.php');

        $this->repositoryService->shouldReceive('generateRepositoryStub')
            ->once()
            ->with(\Mockery::on(function (Repository $repository) {
                $this->assertSame('ModelRepository', $repository->getName());
                $this->assertSame('App\\Repositories\\Path\\To\\ModelRepository', $repository->getFqn());
                return true;
            }))->andReturn('RepositoryStub');

        $this->repositoryService->shouldReceive('createFileFromStubContents')
            ->once()
            ->with('RepositoryStub', 'Repositories/Path/To/ModelRepository.php');

        $this->output->shouldReceive('writeln')
            ->once()
            ->with('<info>Created Repository: Repositories/Path/To/ModelRepository.php</info>', 32);
        $this->output->shouldReceive('writeln')
            ->once()
            ->with('<info>Created Repository Contract: Contracts/Repositories/Path/To/ModelRepositoryContract.php</info>', 32);
        $this->output->shouldReceive('writeln');

        $this->command->handle();
    }
}
