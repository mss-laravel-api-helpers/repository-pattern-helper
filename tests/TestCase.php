<?php
/**
 * Base test case
 */

namespace Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\DB;

/**
 * Class TestCase
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests
 */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function setUp(): void
    {
        parent::setUp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }
}
